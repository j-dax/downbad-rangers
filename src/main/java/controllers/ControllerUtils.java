package controllers;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import io.javalin.http.Context;

public class ControllerUtils {
    public static void printContext(Context ctx) {
        Map<String, String[]> paramMap = ctx.req.getParameterMap();
        System.out.println("**** Request Params *****");
        if (paramMap.size() > 0) {
            printMap(paramMap);
        }

        Enumeration<String> ctxEnum = ctx.req.getHeaderNames();
        System.out.println("**** Request Header *****");
        if (ctxEnum.hasMoreElements()) {
            while (ctxEnum.hasMoreElements()) {
                String nextEle = ctxEnum.nextElement();
                System.out.println(nextEle);
            }
        }

        String body = ctx.body();
        System.out.println("**** Request Body *****");
        if (body.length() > 0) {
            System.out.println(body);
        }

        Map<String, List<String>> formParamMap = ctx.formParamMap();
        System.out.println("**** Request Form *****");
        if (formParamMap.size() > 0) {
            printMap(formParamMap);
        }
    }

    public static <K, V> void printMap(Map<K, V> map) {
        for (Entry<K, V> e : map.entrySet()) {
            System.out.print(e.getKey().toString());
            System.out.print(": ".toString());
            System.out.println(e.getValue().toString());
        }
    }

    public static HashMap<String, String> mapFromJson(String json) {
        HashMap<String, String> paramMap = new HashMap<String, String>();
        
        // splice the {" ... "} from the ends, assume the rest is flat json -- no more embedded objects
        if (json.length() > 2) 
            json = json.substring(1, json.length() - 1);
        else
            return paramMap;

        String[] bodyList = json.split(",");
        for (int i = 0; i < bodyList.length; i++) {
            String[] params = bodyList[i].split(":");
            if (params.length > 1) {
                String key = params[0];
                String value = params[1];
                if (key.length() > 2 && value.length() > 2) {
                    key = key.substring(1, key.length() - 1);
                    value = value.substring(1, value.length() - 1);
                    paramMap.put(key, value);
                }
            }
        }
        return paramMap;
    }

    public static String strip(String s) {
        if (s == null) return "";
        
        int i_min = 0;
        for (; i_min < s.length(); i_min++) {
            if (s.charAt(i_min) == ' ') continue;
            break;
        }

        int i_max = s.length() - 1;
        for (; i_max > 0; i_max--) {
            if (s.charAt(i_max) == ' ') continue;
            break;
        }
        
        return s.substring(i_min, i_max + 1);
    }
}
