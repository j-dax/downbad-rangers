package controllers;

import io.javalin.http.Context;
import oauth.TwitterOAuth;
import services.TwitterService;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.*;

import config.LoggerConfig;

public class TwitterController {
    private static TwitterOAuth t4 = new TwitterOAuth();
    public static AccessToken at = null;
    public static RequestToken rt = null;

    /**
     * Render the login page
     * @param ctx - the context made during a request
     */
    public static void get(Context ctx) {
        ctx.render("/login.html");
    }

    /**
     * https://developer.twitter.com/en/docs/authentication/guides/log-in-with-twitter
     * Obtain a request token, store a verification token and redirect the client
     * 
     * @param ctx - the context made during a request
     */
    public static void twitterLogin(Context ctx) {
        // obtain token and token secret
        class TwitterLoginJson {
            String redirect;
            TwitterLoginJson(String r) {
                redirect = r;
            }
            @JsonProperty("redirect")
            String getRedirect() { return redirect; }
        }

        RequestToken rt = t4.getApplicationRequestToken();
        String tok = rt.getToken();
        ctx.sessionAttribute("twitter_request_token", rt);
        TwitterController.rt = rt;
        ctx.sessionAttribute("oauth_verifier", tok);

        TwitterLoginJson obj = new TwitterLoginJson("https://api.twitter.com/oauth/authenticate?oauth_token=" + tok);
        ctx.json(obj);
    }

    /**
     * Final step in the 3-legged oauth chain for twitter
     * Verifies that url params contain `oauth_verifier` and `oauth_token`
     *      and further checks that the token is the same from the first step
     * @param ctx - the context made during a request
     */
    public static void twitterAuthenticated(Context ctx) {
        // Request params must have both oauth_verifier and oauth_token
        String verifier = ctx.req.getParameter("oauth_verifier");
        String token = ctx.req.getParameter("oauth_token");
        at = t4.getAccessToken(rt, verifier);

        LoggerConfig.log(TwitterController.class.getSimpleName(), "Token   : " + token);
        LoggerConfig.log(TwitterController.class.getSimpleName(), "Original: " + ctx.sessionAttribute("oauth_verifier"));
        if (verifier != null && token != null && token.equals(ctx.sessionAttribute("oauth_verifier"))) {
            // verifier checks out, log in and save user credentials
            long id = t4.getProfileId(at);
            // save credentials
            String username = "TwitterUser-" + String.valueOf(id);
            ctx.sessionAttribute("session_username", username);
            UserController.userString = username;
            if (TwitterService.userExists(username)) {
                LoggerConfig.log(TwitterController.class.getSimpleName(), "Authenticated existing user");
                TwitterService.updateUser(username, token, verifier);
            } else {
                LoggerConfig.log(TwitterController.class.getSimpleName(), "Authenticated new user");
                TwitterService.createTwitterUser(username, token, verifier);
            }
            LoggerConfig.log(TwitterController.class.getSimpleName(), "Authenticated endpoint: redirect /renew");
            ctx.redirect("/home");
            return;
        }
        LoggerConfig.log(TwitterController.class.getSimpleName(), "Unauthenticated endpoint: redirect /login");
        ctx.redirect("/login");
    }

    public static void shareOnTwitter(Context ctx) {
        String username = ctx.sessionAttribute("session_username");
        if (username == null) username = UserController.userString;
        if (TwitterService.userExists(username) && at != null) {
            // do post to twitter
            String message = ctx.formParam("message");
            t4.setStatus(at, message);
            ctx.redirect("/collections");
        } else {
            ctx.redirect("/logout");
        }
    }
}
