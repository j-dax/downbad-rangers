package controllers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import io.javalin.http.Context;
import io.javalin.http.UploadedFile;
import services.UserService;
import services.UserServiceImpl;
import org.apache.commons.io.FileUtils;

import config.LoggerConfig;

// import com.google.common.base.Splitter;

public class UserController {
	static UserService userService = new UserServiceImpl();

	/*
	 * This method will provide login functionality using the formParam
	 */
	public static void loginUser(Context context) {
		if (context.method() == "POST") {
			String username = null;
			String password = null;

			String body = context.body();
			HashMap<String, String> paramMap = null;
			if (body.length() > 2) {
				paramMap = ControllerUtils.mapFromJson(body);
				username = paramMap.get("signin-username");
				password = paramMap.get("signin-password");
			}
			
			boolean login = userService.loginUser(username, password);
			if (username != null && password != null && login) {
				LoggerConfig.log(UserController.class.getSimpleName(), "User logged in: " + username);
				context.sessionAttribute("session_username", username);
				userString = username;
				context.json("{\"Success\": \"login successful\"}").status(200);
			} else {
				LoggerConfig.log(UserController.class.getSimpleName(), "User login failed, username: " + username);
				context.json("{}").status(400);
			}
		} else {
			context.render("/login/login.html");
		}
	}

	/*
	 * This method will provide signup functionality using the formParam
	 */
	public static void signUpUser(Context context) {
		LoggerConfig.log(UserController.class.getSimpleName(), context.sessionAttribute("session_username"));
		if (context.method() == "POST") {
			String username = null;
			String password = null;
			String email = null;

			String body = context.body();
			HashMap<String, String> paramMap = null;
			if (body.length() > 2) {
				paramMap = ControllerUtils.mapFromJson(body);
				username = paramMap.get("signup-username");
				password = paramMap.get("signup-password");
				email = paramMap.get("signup-email");
			}

			if (username != null && password != null && email != null && userService.getUserId(username) == -1) {
				LoggerConfig.log(UserController.class.getSimpleName(), "User signup, username: " + username);
				userService.signUpUser(username, password, email);
				context.sessionAttribute("session_username", username);
				userString = username;
				context.status(201);
			} else {
				LoggerConfig.log(UserController.class.getSimpleName(), "User failed signup");
				context.status(400);
			}
		} else {
			context.render("/login/login.html");
		}
	}

	public static void home(Context context) {
		context.render("/home/index.html");
	}

	public static void about(Context context) {
		context.render("/about/about.html");
	}

	/*
	 * This method will provide encoding functionality using the file that the user
	 * uploads
	 */
	public static void encode(Context context) {
		if (context.method() == "POST") {
			String username = context.sessionAttribute("session_username");
			if (username == null) username = userString;
			int user_id = userService.getUserId(username);
			String message = context.formParam("message");
			message = ControllerUtils.strip(message);
			UploadedFile file = context.uploadedFile("myFile");
			long size = file.getSize();

			if (message.length() == 0 || size == 0) {
				context.status(400);
				return;
			}

			int img_id = userService.getNextImgId();

			try {
				String filepath = "src/main/resources/images/";
				// Naming construct user_id_img_id_filename
				String normal_img = user_id + "_" + img_id + "_0_" + file.getFilename();
				String encoded_img = user_id + "_" + img_id + "_1_" + file.getFilename();
				File f = new File(filepath + normal_img);
				
				LoggerConfig.log(UserController.class.getSimpleName(), "uploading file as image" + f.getAbsolutePath());

				FileUtils.copyInputStreamToFile(file.getContent(), f);

				// Perform the encoding
				userService.encode(filepath, normal_img, "png", encoded_img, message, user_id);

				File localFile = new File(filepath + encoded_img);
				InputStream inputStream = new BufferedInputStream(new FileInputStream(localFile));
				context.header("Content-Disposition", "attachment; filename=\"" + localFile.getName() + "\"");
				context.header("Content-Length", String.valueOf(localFile.length()));
				context.result(inputStream).contentType("image/png");
			} catch (IOException e) {
				System.out.println("Unable to save image!");
				e.printStackTrace();
			}
		} else {
			context.render("/encode/encrypt.html");
		}
	}

	public static String userString = null;

	/*
	 * This method will provide decoding functionality using the file that the user
	 * uploads
	 */
	public static void decode(Context context) {
		if (context.method() == "POST") {
			UploadedFile file = context.uploadedFile("myFile");
			LoggerConfig.log(UserController.class.getSimpleName(), "decoding image:" + file.getFilename());
			
			String filepath = "src/main/resources/images/";
			String secret_message = userService.decode(filepath, file.getFilename());
			context.contentType("application/json");
			context.result("{\"message\":\"" + secret_message + "\"}");
		} else {
			context.render("/decode/decrypt.html");
		}
	}

	/*
	 * This method will provide viewing functionality to view all of the images for
	 * the a specific user
	 */
	public static void collection(Context context) {
		if (context.method() == "POST") {
			// I am going to return json that contains {img_id: img_link}
			String username = context.sessionAttribute("session_username");
			if (username == null) username = userString;
			int user_id = userService.getUserId(username);

			Map<Integer, String> myImages = userService.getImages(user_id);
			LoggerConfig.log(UserController.class.getSimpleName(), "Viewing collection of " + String.valueOf(myImages.size()) + " images.");
			context.json(myImages);
		} else {
			context.render("/collections/collection.html");
		}
	}

	public static void changepw(Context context) {
		if(context.method().equals("POST")) {
			String username = context.sessionAttribute("session_username");
			String currentPass = null;
			String newPass1 = null;
			String newPass2 = null;
			
			String body = context.body();
			HashMap<String, String> paramMap = null;
			if (body.length() > 2) {
				paramMap = ControllerUtils.mapFromJson(body);
				currentPass = paramMap.get("curpass");
				newPass1 = paramMap.get("np1");
				newPass2 = paramMap.get("np2");
			}
			
			int user_id = userService.getUserId(username);
			String password = userService.getPass(user_id);
			
			if(password.equals(currentPass) && newPass1.equals(newPass2)) {
				userService.changepw(user_id, newPass1);
				context.json("{\"status\":\"ok\"}").status(200);
			}
			else {
				context.json("{\"status\":\"error\"}").status(400);
			}
		}
		else {
			context.render("/chpass/chpass.html");
		}
	}

	public static void logout(Context context) {
		context.req.getSession().invalidate();
		userString = null;
		TwitterController.rt = null;
		context.redirect("/login");
	}
	
	public static void EditProfile(Context context) {
		if (context.method().equals("POST")) {
			String username = context.sessionAttribute("session_username");
			if (username == null) username = userString;
			String password = "null";
			String newUsername = "null";
			String newEmail = "null";

			String body = context.body();
			HashMap<String, String> paramMap = null;
			if (body.length() > 2) {
				paramMap = ControllerUtils.mapFromJson(body);
				password = paramMap.get("user_pass");
				newUsername = paramMap.get("user_name");
				newEmail = paramMap.get("user_email");
			}
			
			
			int user_id = userService.getUserId(username);
			String currentPass = userService.getPass(user_id);
			if(password.equals(currentPass)) {
				userService.changeProfile(user_id, password, newUsername, newEmail);
				context.sessionAttribute("session_username", newUsername);
				context.json("{\"status\":\"ok\"}").status(200);
			}
			else {
				context.json("{\"status\":\"error\"}").status(400);
			}
			// This needs to return a boolean so that the client knows what page to load
			
		}
		else if(context.method().equals("PUT")) {
			String username = context.sessionAttribute("session_username");
			if (username == null) username = userString;
			String email = userService.getEmail(username);
			System.out.println("{\"email\":\"" + email + "\", \"username\":\"" + username + "\"}");
			context.contentType("application/json");
			context.result("{\"email\":\"" + email + "\", \"username\":\"" + username + "\"}");
		}
		else{
			context.render("/profile/EditProfile.html");
		}
	}
	
	
	public static void deleteAcc(Context context) {
		String username = context.sessionAttribute("session_username");
		if (username == null) username = userString;

		LoggerConfig.log(UserController.class.getSimpleName(), "deleteAcc called on \"" + username + "\"");

		userService.deleteAcc(username);
		context.json("{\"status\":\"ok\"}").status(200);
	}
	
	
	public static void getCreds(Context context) {
		String username = context.sessionAttribute("session_username");
		if (username == null) username = userString;
		String email = userService.getEmail(username);
		System.out.println("{\"email\":\"" + email + "\", \"username\":\"" + username + "\"}");
		context.contentType("application/json");
		context.result("{\"email\":\"" + email + "\", \"username\":\"" + username + "\"}");
	}
	
	
	
}
