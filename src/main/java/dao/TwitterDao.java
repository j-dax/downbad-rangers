package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dbconfig.ConnectionUtil;
import dbconfig.ResourceClosers;

public class TwitterDao {
	public static boolean userExists(String username) {
		Connection conn = null;
        PreparedStatement stmt = null;
		ResultSet rs = null;
        boolean exists = false;
		
		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "SELECT * FROM stegano_users WHERE user_name = ?";
			stmt = conn.prepareStatement(SQL);
            stmt.setString(1, username);
            rs = stmt.executeQuery();

			if (rs.next())
				exists = true;
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
			ResourceClosers.closeResultSet(rs);
		}
        return exists;
    }

	public static void createTwitterUser(String username, String token, String verifier) {
		Connection conn = null;
		PreparedStatement stmt = null;
        ResultSet rs = null;

        final String userSQL = "INSERT into stegano_users VALUES(default, ?, ?, ?) RETURNING user_id";
        final String twitterSQL = "INSERT into Stegano_BirdPeople VALUES(?, ?, ?, ?)";
		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			stmt = conn.prepareStatement(userSQL);
			stmt.setString(1, username);
			stmt.setString(2, "password");
			stmt.setString(3, "email");

			rs = stmt.executeQuery();
			rs.next();
			Integer id = rs.getInt("user_id");
            ResourceClosers.closeStatement(stmt);
            ResourceClosers.closeResultSet(rs);
            
			stmt = conn.prepareStatement(twitterSQL);
			stmt.setInt(1, id);
			stmt.setString(2, username);
			stmt.setString(3, token);
			stmt.setString(4, verifier);
			stmt.execute();
            ResourceClosers.closeStatement(stmt);
            ResourceClosers.closeConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> getUserTokens(String username) {
		ArrayList<String> list = new ArrayList<>();

		Connection conn = null;
		PreparedStatement stmt = null;
        ResultSet rs = null;

        final String twitterSQL = "SELECT user_token, user_verifier FROM Stegano_BirdPeople WHERE user_name LIKE ?";
		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			stmt = conn.prepareStatement(twitterSQL);
			stmt.setString(1, username);

			rs = stmt.executeQuery();
			if (rs.next()) {
				list.add(rs.getString("user_token"));
				list.add(rs.getString("user_verifier"));
			}

            ResourceClosers.closeResultSet(rs);
            ResourceClosers.closeStatement(stmt);
            ResourceClosers.closeConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return list;
    }
	
	public static void updateUser(String username, String token, String verifier) {
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "UPDATE Stegano_BirdPeople SET user_token = ?, user_verifier = ? WHERE user_name = ?";
			stmt = conn.prepareStatement(SQL);

			stmt.setString(1, token);
			stmt.setString(2, verifier);
			stmt.setString(3, username);

			stmt.execute();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
	}
	
    public static void deleteUser(String username) {
		Connection conn = null;
        PreparedStatement stmt = null;
		
		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "DELETE FROM Stegano_BirdPeople WHERE user_name = ?";
			stmt = conn.prepareStatement(SQL);
			stmt.setString(1, username);
            stmt.execute();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
    }
    
}
