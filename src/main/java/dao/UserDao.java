package dao;

import java.util.Map;
/*
 * define how the implementations are going to be for the DAO
 */

public interface UserDao {
	boolean loginUser(String username, String password);
	void signUpUser(String username, String password, String email);
	public void encode(String filepath, int user_id);
	int getUserId(String username);
	int getNextImgId();
	Map<Integer, String> getImages(int user_id);
	void changepw(int user_id, String password);
	void changeUsername(int user_id, String password, String username);
	void changeEmail(int user_id, String password, String email);
	String getEmail(String username);
	String getPass(int user_id);
	void deleteAcc(int user_id);
	void deleteByName(String user_name);
}
