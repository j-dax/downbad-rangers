package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import config.LoggerConfig;
import dbconfig.ConnectionUtil;
import dbconfig.ResourceClosers;


public class UserDaoImpl implements UserDao{
	
	public UserDaoImpl() {
		
	}
	
	@Override
	public boolean loginUser(String username, String password) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet result = null;
		
		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "SELECT * FROM stegano_users where user_name=? AND user_pass=?";
			stmt = conn.prepareStatement(SQL);

			stmt.setString(1, username);
			stmt.setString(2, password);
			
			result = stmt.executeQuery();
			
			boolean exists = result.next();

			return exists;
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
		return false;
	}
	
	@Override
	public void signUpUser(String username, String password, String email) {
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "INSERT into stegano_users VALUES(default, ?, ?, ?)";
			stmt = conn.prepareStatement(SQL);

			stmt.setString(1, username);
			stmt.setString(2, password);
			stmt.setString(3, email);

			stmt.execute();
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
	}
	
	
	@Override
	public void encode(String filepath, int user_id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "INSERT into stegano_images VALUES(?, default, ?)";
			stmt = conn.prepareStatement(SQL);

			stmt.setInt(1, user_id);
			stmt.setString(2, filepath);

			stmt.execute();
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
	}
	
	@Override
	public int getUserId(String username) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet result = null;
		int return_val = -1;
		
		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "Select user_id from stegano_users where user_name=?";
			stmt = conn.prepareStatement(SQL);

			stmt.setString(1, username);

			result = stmt.executeQuery();
			
			result.next();
			
			return_val = result.getInt("user_id");
			
		} catch(SQLException e) {
			// e.printStackTrace();
			LoggerConfig.log(UserDaoImpl.class.getSimpleName(), "SQL Exception, user was not found");
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
		return return_val;
	}
	
	
	@Override
	public int getNextImgId() {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet result = null;
		int return_val = 0;
		
		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "Select max(img_id) as max_img_id from stegano_images";
			stmt = conn.prepareStatement(SQL);
			result = stmt.executeQuery();
			if(result.next()) {
				return_val = result.getInt("max_img_id") + 1;
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
		return return_val;
	}
	
	
	
	@Override
	public Map<Integer, String> getImages(int user_id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet result = null;
		Map<Integer,String> images = new HashMap<Integer, String>();
		
		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "Select img_id, img_link from stegano_images where user_id=?";
			stmt = conn.prepareStatement(SQL);
			
			stmt.setInt(1, user_id);
			result = stmt.executeQuery();

			while(result.next()) {
				images.put(result.getInt("img_id"), result.getString("img_link"));
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
		return images;
	}
	
	
	@Override
	public void changepw(int user_id, String password) {
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "Update stegano_users set user_pass=? where user_id=?";
			stmt = conn.prepareStatement(SQL);
			stmt.setString(1, password);
			stmt.setInt(2, user_id);
			stmt.execute();
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
	}
	
	@Override
	public void changeUsername(int user_id, String password, String username) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "Update stegano_users set user_name=? where user_id=? and user_pass=?";
			stmt = conn.prepareStatement(SQL);
			stmt.setString(1, username);
			stmt.setInt(2, user_id);
			stmt.setString(3, password);
			stmt.execute();

		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
	}

	@Override
	public void changeEmail(int user_id, String password, String email) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "Update stegano_users set email=? where user_id=? and user_pass=?";
			stmt = conn.prepareStatement(SQL);
			stmt.setString(1, email);
			stmt.setInt(2, user_id);
			stmt.setString(3, password);
			stmt.execute();

		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
	}
	
	
	@Override
	public String getEmail(String username) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet result = null;
		String result_val = "";

		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "Select email from stegano_users where user_name=?";
			stmt = conn.prepareStatement(SQL);
			stmt.setString(1, username);
			result = stmt.executeQuery();

			result.next();
			result_val = result.getString("email");
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
		return result_val;
	}
	
	
	@Override
	public String getPass(int user_id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet result = null;
		String result_val = "";

		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "Select user_pass from stegano_users where user_id=?";
			stmt = conn.prepareStatement(SQL);
			stmt.setInt(1, user_id);
			result = stmt.executeQuery();

			result.next();
			result_val = result.getString("user_pass");
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
		return result_val;
	}
	
	
	@Override
	public void deleteAcc(int user_id) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "Delete from stegano_users where user_id=? CASCADE";
			stmt = conn.prepareStatement(SQL);
			stmt.setInt(1, user_id);
			stmt.execute();

			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
	}
	
	public void deleteByName(String user_name) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			//Establish the connection to the DB
			conn = ConnectionUtil.getConnection();
			final String SQL = "Delete from stegano_users where user_name LIKE ?";
			stmt = conn.prepareStatement(SQL);
			stmt.setString(1, user_name);
			stmt.execute();

			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			ResourceClosers.closeConnection(conn);
			ResourceClosers.closeStatement(stmt);
		}
	}

}
