package dbconfig;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;

// This will only be testable if you add
/*
	Public static void main(String[] args) {} on this java file but runs as an object on main for now.
 */
public class ConnectionUtil {

	public static Connection getConnection() throws SQLException {
			Connection con = null;
		try {
			Class.forName("org.postgresql.Driver");
//			String hostname =  "localhost";
//			String port = "5432";
//			String userName = System.getenv("TEST_DB_USER");
//			String password = System.getenv("TEST_DB_PASS");
//			String dbName = System.getenv("DB_NAME");
			
			String hostname =  "localhost";
			String userName = "postgres";
			String password = "toor";
			String port = "5432";
			String dbName = "postgres";
			// Parameters for setting up the jdbcURL loaded in to connect to database currently set as postgres (anyone can access)
			String jdbcUrl = "jdbc:postgresql://" + hostname + ":" + port + "/" + dbName + "?user=" + userName + "&password=" + password;
			// jdbc setup for connection url to pass as a whole
			con = DriverManager.getConnection(jdbcUrl);
			return con;
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}