package frontcontroller;

import controllers.UserController;
import controllers.TwitterController;
import io.javalin.Javalin;

import config.LoggerConfig;

public class Dispatcher {
	
	public Dispatcher(Javalin app) {
		
		// Login and signup
		app.get("/", UserController::loginUser);
		app.get("/login", UserController::loginUser);
		app.post("/login", UserController::loginUser);
		app.post("/signup", UserController::signUpUser);
		app.get("/logout", UserController::logout);
		
		// Changing password functionality
		app.put("/changepw", UserController::changepw);
		
		// Get pages
		app.get("/home", UserController::home);
		app.get("/collections", UserController::collection);
		app.post("/collections", UserController::collection);
		app.get("/about", UserController::about);
		
		// Encoding and decoding
		app.post("/encode", UserController::encode);
		app.get("/encode", UserController::encode);
		app.get("/decode", UserController::decode);
		app.post("/decode", UserController::decode);
		
		//edit profile
		app.get("/EditProfile", UserController::EditProfile);
		app.post("/EditProfile", UserController::EditProfile);
		app.put("/EditProfile", UserController::EditProfile);
		
		
		app.get("/ChangePassword", UserController::changepw);
		app.post("/ChangePassword", UserController::changepw);
		
		app.delete("/deleteAcc", UserController::deleteAcc);
		
		app.get("/getCreds", UserController::getCreds);


		// Twitter OAuth
		app.get("/twitter/login", TwitterController::twitterLogin);
		app.get("/twitter/authenticated", TwitterController::twitterAuthenticated);
		app.post("twitter/share", TwitterController::shareOnTwitter);

		app.exception(Exception.class, (e, ctx) -> {
			LoggerConfig.log(Dispatcher.class.getSimpleName(), "Exception thrown: " + e.getClass().getSimpleName());
			ctx.render("/404/404.html");
		});
	}

}
