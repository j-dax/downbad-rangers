package oauth;

import java.io.File;
import java.io.InputStream;

import config.LoggerConfig;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterOAuth {
    private final String TWITTER_CONSUMER_KEY = "a6vQDG6zCD0HjArC2sL0AWnzP";
    private final String TWITTER_CONSUMER_SECRET = "4X0uZ1x5ArkiuL3jD1brGUJlN95zMKQpZH8jN3lcp2e6EvlXlr";
    // private final String TWITTER_ACCESS_KEY = System.getenv("TWITTER_ACCESS_KEY");
    // private final String TWITTER_ACCESS_SECRET = System.getenv("TWITTER_ACCESS_SECRET");
    // private final String TWITTER_BEARER_TOKEN = System.getenv("TWITTER_BEARER_TOKEN");

    ConfigurationBuilder cb;
    TwitterFactory tf;

    public TwitterOAuth() {
        cb = new ConfigurationBuilder()
        .setOAuthConsumerKey(TWITTER_CONSUMER_KEY).setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET)
        // .setOAuth2AccessToken(TWITTER_ACCESS_KEY).setOAuthAccessTokenSecret(TWITTER_ACCESS_SECRET)
        .setDebugEnabled(true).setHttpConnectionTimeout(100000);
        tf = new TwitterFactory(cb.build());
    }
    
    public RequestToken getApplicationRequestToken() {
        try {
            Twitter twitter = tf.getInstance();
            RequestToken requestToken = twitter.getOAuthRequestToken();
            return requestToken;
        } catch (TwitterException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Status createTweet(String status) {
        try {
            Twitter twitter = tf.getInstance();
            return twitter.updateStatus(status);
        } catch (TwitterException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Status createTweet(String status, String uri) {
        try {
            StatusUpdate su = new StatusUpdate(status);
            // must be a twitter-specific link, or this throws IllegalStateException
            su.attachmentUrl(uri); 
            Twitter twitter = tf.getInstance();
            return twitter.updateStatus(su);
        } catch (TwitterException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public AccessToken getAccessToken(RequestToken rt, String token) {
        AccessToken at = null;
        try {
            at = tf.getInstance().getOAuthAccessToken(rt, token);
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        return at;
    }

    public long getProfileId(AccessToken at) {
        try {
            long id =  tf.getInstance(at).getId();
            LoggerConfig.log(TwitterOAuth.class.getSimpleName(), "Logging in: TwitterUser-" + String.valueOf(id));
            return id;
        } catch (TwitterException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public void setStatus(AccessToken at, String status) {
        try {
            tf.getInstance(at).updateStatus(status);
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

}
