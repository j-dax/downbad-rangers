package services;

import java.util.ArrayList;

import dao.TwitterDao;

public class TwitterService {
    public static boolean userExists(String username) {
        return TwitterDao.userExists(username);
    }

    public static void createTwitterUser(String username, String token, String verifier) {
        TwitterDao.createTwitterUser(username, token, verifier);
    }

    public static ArrayList<String> getUserTokens(String username) {
        return TwitterDao.getUserTokens(username);
    }

    public static void updateUser(String username, String token, String verifier) {
        TwitterDao.updateUser(username, token, verifier);
    }

    public static void deleteUser(String username) {
        TwitterDao.deleteUser(username);
    }
}
