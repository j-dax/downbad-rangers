package services;


import java.util.ArrayList;
import java.util.Map;
import stegan.Steganography;
import models.User;

public interface UserService {

	boolean loginUser(String username, String password);
	void signUpUser(String username, String password, String email);
	public boolean encode(String filepath, String normal_img, String ext, String encoded_img, String message, int user_id);
	String decode(String filepath, String filename);
	int getUserId(String username);
	int getNextImgId();
	Steganography stegan();
	Map<Integer, String> getImages(int user_id);
	void changepw(int user_id, String password);
	void changeProfile(int user_id, String password, String newUsername, String newEmail);
	String getEmail(String username);
	String getPass(int user_id);
	void deleteAcc(int user_id);
	void deleteAcc(String user_name);
	
}
