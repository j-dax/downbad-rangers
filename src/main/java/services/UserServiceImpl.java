package services;

import java.util.ArrayList;
import java.util.Map;

import dao.UserDao;
import dao.UserDaoImpl;
import stegan.Steganography;

public class UserServiceImpl implements UserService{

	UserDao userDao;
	
	public UserServiceImpl() {
		this.userDao = new UserDaoImpl();
	}
	
	@Override
	public boolean loginUser(String username, String password) {
		return this.userDao.loginUser(username, password);
	}
	
	@Override
	public void signUpUser(String username, String password, String email) {
		this.userDao.signUpUser(username, password, email);
	}
	
	@Override
	public Steganography stegan() {
		Steganography stegan = new Steganography();
		return stegan;
	}
	
	@Override
	public boolean encode(String filepath, String normal_img, String ext, String encoded_img, String message, int user_id) {
		// We don't really need to create an object here. We can just refactor and change the methods to static
		
		
		boolean result = stegan().encode(filepath, normal_img, ext, encoded_img, message);
		
		String swag = stegan().decode(filepath, encoded_img);
		System.out.println(swag);
		
		if(result) {
			// This adds the image link and respective user_id into the database
			this.userDao.encode(encoded_img, user_id);
		}

		return result;
	}
	
	@Override
	public String decode(String filepath, String filename) {
		// We don't really want to store anything on the database -- so lets not call the DAO
		return stegan().decode(filepath, filename);
	}
	
	
	@Override
	public int getUserId(String username) {
		return this.userDao.getUserId(username);
	}
	
	@Override
	public int getNextImgId() {
		return this.userDao.getNextImgId();
	}
	
	
	@Override
	public Map<Integer, String> getImages(int user_id) {
		return this.userDao.getImages(user_id);
	}
	
	@Override
	public void changepw(int user_id, String password) {
		this.userDao.changepw(user_id, password);
	}
	
	@Override
	public void changeProfile(int user_id, String password, String newUsername, String newEmail) {
		if(!newUsername.equals("")) {
			this.userDao.changeUsername(user_id, password, newUsername);
			
		}
		if(!newEmail.equals("")) {
			System.out.println("Hello?");
			this.userDao.changeEmail(user_id, password, newEmail);
		}
	}
	
	@Override
	public String getEmail(String username) {
		return this.userDao.getEmail(username);
	}
	
	@Override
	public void deleteAcc(int user_id) {
		this.userDao.deleteAcc(user_id);
	}
	
	
	@Override
	public String getPass(int user_id) {
		return this.userDao.getPass(user_id);
	}

	@Override
	public void deleteAcc(String user_name) {
		this.userDao.deleteByName(user_name);
	}


	
	
	
	
	

}
