// card = document.getElementById("activator")
// navbar = document.getElementById("animatebar")
//      var card = document.getElementById('activator');
//         var tl = gsap.timeline({defaults: {ease: "power2.inOut"}})

//         var toggle = false;

//         tl.to('.activator', {
//             // background: '#805ad5',
//             'borderTopRightRadius': '0',
//             'borderBottomRightRadius': '0',
           
//         });
//         tl.to('nav', {
//             'clipPath': 'ellipse(100% 100% at 50% 50%)'
//         }, "-=.3")
//         tl.to('nav img', {
//             opacity: 1,
//             transform: 'translateX(0)',
//             stagger: .05
//         }, "-=.3")
//         tl.pause();

//         card.addEventListener('click', () => {
//             toggle = !toggle;
//             if (toggle ? expand()  : reduce());
//         })

//         function expand(){
//             tl.play();
//             navbar.style.transition = "ease-in-out .7s";
//             navbar.style.transform  = "translateX(8px)"
            
//         }

//         function reduce(){
//             tl.reverse();
//             navbar.style.transition = "ease-in-out .7s";
//             navbar.style.transform  = "translateX(250px)"
//             // card.style.tranform = translateX(-500);
//         }

function menuToggle(){
    const toggleMenu = document.querySelector('.menu');
    toggleMenu.classList.toggle('active');
  }
  
  
  function confirmDelete(){
    let answer = window.confirm(" Are you sure you want to delete your account?");
    if (answer){
    	let url = "http://localhost:9001/deleteAcc"
      	fetch(url, {
		    method: 'DELETE'})
		  .then(response => response.json())
		  .then(data => {
			console.log(data)
		  })
		window.location.replace("http://localhost:9001/login")
    }
    else {
      // None
    }
  }
  
async function getUserCredentials() {
    let url = "http://localhost:9001/getCreds"
    let response = await fetch(url)

    let data = await response.json()
    
    let pemail = document.getElementById("profileEmail")
	let puser = document.getElementById("profileUser")
	
	
	console.log(data);
	
	pemail.appendChild( document.createTextNode(data["email"]) );
	puser.appendChild( document.createTextNode(data["username"]) );
	
}

window.onload = function() {
    this.getUserCredentials();
}