fetch('http://localhost:9001/collections', {
  method: 'POST',
  headers: {
    credentials: "include",
  }
})
  .then(response => response.json())
  .then(data => {
    let i = 0;
    for (var key in data) {
      // find slides class div 
      let slider_images = document.querySelector(".slides");
      let slider_labels = document.querySelector(".slide-buttons");
      
      let content = data[key];
      let create_div = createDiv(content, "slide-" + (i+1));
      let create_a = createAnchor("#slide-"+(i+1), "slideC-"+(i+1));
      create_a.onclick = ()=>shareModal(content);

      slider_images.appendChild(create_div);
      slider_labels.appendChild(create_a);

      document.querySelector("#slideC-" + (i+1)).innerHTML = (i+1);

      i = i + 1;
    };
    // no elements added, let's tell the user to add an image
    if (i === 0) {
      // remove content from page
      let contentArea = document.querySelector(".container-row");
      contentArea.innerHTML = "";

      let warn = document.createElement("div");
      warn.classList.add("slider"); // for text-align center and width/height
      warn.style.fontSize = "xx-large";
      warn.innerHTML = `
        <p>Looks like you haven't added any images yet</p>
        <a href="/encode"><button class="encrypt-button-full">Start here</button></a>
      `;

      contentArea.appendChild(warn);
    }
  });

function createModal() {
  let modalCover = document.createElement("div");
  modalCover.style.position = "absolute";
  modalCover.style.width = "100%";
  modalCover.style.height = "100%";
  modalCover.style.background = "rgba(0,0,0,0.5)";
  modalCover.style.top = "0";
  modalCover.style.display = "flex";
  modalCover.style.alignItems = "center";
  modalCover.style.justifyContent = "center";
  modalCover.style.padding = "32px";
  document.body.appendChild(modalCover);
  return modalCover;
}

function innerModal(url) {
  let inner = document.createElement("div");
  inner.style.backgroundColor = "white";
  inner.style.width = "60%";
  inner.style.height = "80%";
  
  inner.style.display = "flex";
  inner.style.flexDirection = "column";
  inner.style.alignItems = "center";
  inner.style.justifyContent = "center";

  return inner;
}

function shareForm(url) {
  let form = document.createElement("form");
  form.action = `http://localhost:9001/twitter/share`;
  form.method = "POST";

  form.style.display = "flex";
  form.style.flexFlow = "column nowrap";
  form.style.justifyContent = "space-around";
  form.style.alignItems = "center";

  form.style.height = "100%";
  form.style.width = "100%";
  form.style.padding = "32px";
  return form;
}

function shareImage(url) {
  let img = document.createElement("div");
  img.style.backgroundImage = `url("/${url}")`;
  img.style.backgroundRepeat = "no-repeat";
  img.style.backgroundSize = "contain";
  img.style.backgroundPositionX = "center";
  img.style.height = "80%";
  img.style.width = "60%";
  return img;
}

function shareMessage(url) {
  let textArea = document.createElement("textarea");
  textArea.name = "message";
  textArea.value = `I just hid a message in this image! Check it out. http://localhost:9001/${url}`;
  return textArea;
}

function shareModal(url) {
  let innerForm = shareForm(url);

  let img = shareImage(url);
  innerForm.appendChild(img);

  let textArea = shareMessage(url);
  innerForm.appendChild(textArea);

  let input = document.createElement("input");
  input.type = "text";
  input.name = "filename";
  input.value = url;
  input.style.display = "none";
  innerForm.appendChild(input);

  let submitButton = document.createElement("input");
  submitButton.type = "submit";
  // submitButton.innerHTML = "<i class='fab fa-twitter'></i>";
  submitButton.value = "Tweet";
  innerForm.appendChild(submitButton);

  let inner = innerModal();
  inner.appendChild(innerForm);

  let modal = createModal();
  modal.appendChild(inner);
  
  function deleteTheseBoys() {
    innerForm.removeChild(img);
    innerForm.removeChild(textArea);
    innerForm.removeChild(input);
    innerForm.removeChild(submitButton);
    inner.removeChild(innerForm);
    modal.removeChild(inner);
    document.body.removeChild(modal);
  }

  modal.onclick = (e)=> {
    if (e.target == modal) {
      deleteTheseBoys();
    }
  };
  button.onclick = deleteTheseBoys();
}

// create buttons to link on these images for quick scrolling
function createAnchor(divRef, id) {
  let slider_labels = document.querySelector(".slide-buttons");
  let create_a = document.createElement("a");
  create_a.setAttribute("href", divRef);
  create_a.setAttribute("id", id);
  return create_a;
}

// Create a div to hold our image
function createDiv(addr, id) {
  let create_div = document.createElement("div");
  create_div.setAttribute("id", id);
  create_div.classList.add("slider-img");
  create_div.style.backgroundImage = `url(/${addr})`;
  create_div.onclick = () => downloadFromLink(`http://localhost:9001/${addr}`, addr);
  return create_div;
}

function downloadFromLink(link, filename) {
  fetch(link, {
    headers: {
      credentials: "include",
    }
  })
  .then(resp=>resp.blob())
  .then(blob=>{
    // add an anchor
    var a = document.createElement("a");
    document.body.appendChild(a);

    var url = window.URL.createObjectURL(blob);
    // set anchor to download target and click
    a.style = "display: none";
    a.href = url;
    a.download = filename;
    a.click();
    // clean up to avoid a leak
    window.URL.revokeObjectURL(url);
    document.body.removeChild(a);
  });
}

function menuToggle(){
  const toggleMenu = document.querySelector('.menu');
  toggleMenu.classList.toggle('active');
}


  function confirmDelete(){
    let answer = window.confirm(" Are you sure you want to delete your account?");
    if (answer){
    	let url = "http://localhost:9001/deleteAcc"
      	fetch(url, {
		    method: 'DELETE'})
		  .then(response => response.json())
		  .then(data => {
			console.log(data)
		  })
		window.location.replace("http://localhost:9001/login")
    }
    else {
      // None
    }
  }
  
async function getUserCredentials() {
    let url = "http://localhost:9001/getCreds"
    let response = await fetch(url)

    let data = await response.json()
    
    let pemail = document.getElementById("profileEmail")
	let puser = document.getElementById("profileUser")
	
	
	console.log(data);
	
	pemail.appendChild( document.createTextNode(data["email"]) );
	puser.appendChild( document.createTextNode(data["username"]) );
	
}

window.onload = function() {
    this.getUserCredentials();
}
