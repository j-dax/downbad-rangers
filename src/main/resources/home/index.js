function menuToggle(){
    const toggleMenu = document.querySelector('.menu');
    toggleMenu.classList.toggle('active');
  }
  
  
function confirmDelete(){
    let answer = window.confirm(" Are you sure you want to delete your account?");
    if (answer){
    	let url = "http://localhost:9001/deleteAcc"
        fetch(url, { method: 'DELETE'})
		   .then(response => response.json())
		   .then(data => {
			 console.log(data)
		   })

      // This isn't correctly triggering the DELETE endpoint
      //let xhr = new XMLHttpRequest();
      //xhr.open("DELETE", url);
      //xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
      //xhr.onreadystatechange = ()=>{
        //if (xhr.readyState == 4) {
          //let json = JSON.parse(xhr.responseText);
          //console.log(json);
        //}
      //}
      
		window.location.replace("http://localhost:9001/login")
    }
  }
  
  
  
async function getUserCredentials() {
    let url = "http://localhost:9001/getCreds"
    let response = await fetch(url)

    let data = await response.json()
    
    let pemail = document.getElementById("profileEmail")
	let puser = document.getElementById("profileUser")
	
	
	console.log(data);
	
	pemail.appendChild( document.createTextNode(data["email"]) );
	puser.appendChild( document.createTextNode(data["username"]) );
	
}

window.onload = function() {
    this.getUserCredentials();
}