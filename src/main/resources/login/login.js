const sign_in_btn = document.querySelector('#sign-in-button');
const sign_up_btn = document.querySelector('#sign-up-button');
const sign_in_form = document.getElementById('sign_in_form');
const panel_right = document.getElementById('pan-r');
// const panel_left = document.getElementById('pan-l');

const container = document.querySelector('.container');

sign_up_btn.addEventListener('click', ()=>{
    container.classList.add('sign-up-mode');
    sign_in_form.style.visibility = "hidden";
    panel_right.style.zIndex = "9";

});

sign_in_btn.addEventListener('click', ()=>{
    container.classList.remove('sign-up-mode');
    panel_right.style.zIndex = "7";
    setTimeout(() => {  sign_in_form.style.visibility = "visible";}, 1200);
});


function setFormMessage(formElement, type, message) {
    const messageElement = formElement.querySelector(".form__message");

    messageElement.textContent = message;
    messageElement.classList.remove("form__message--success", "form__message--error");
    messageElement.classList.add(`form__message--${type}`);
}


function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}

async function postFormDataAsJson(url, formData) {
	// const plainFormData = Object.fromEntries(formData.entries());
	// const formDataJsonString = JSON.stringify(plainFormData);
	// const formDataJsonString = JSON.stringify(formData);

	console.log(formData);
	const fetchOptions = {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			"Accept": "application/json",
			"credentials": "include",
		},
		body: JSON.stringify(formData),
	};
	console.log(fetchOptions);

	const response = await fetch(url, fetchOptions);
	return response.ok;
}


async function handleForm(event, onErrorMessage) {
	event.preventDefault();

	const form = event.currentTarget;
	const url = form.action;

	formJson = {};
	for (ele of form.getElementsByTagName("input")) {
		if (ele.type != "submit")
			formJson[ele.name] = ele.value
	}
    const responseStatus = await postFormDataAsJson(url, formJson);
    if (!responseStatus) {
        setFormMessage(form, "error", onErrorMessage);
    } else {
		window.location.replace("http://localhost:9001/home")
	}
}


const exampleForm = document.getElementById("sign_in_form");
exampleForm.addEventListener("submit", e=>handleForm(e, "Invalid username or password!"));

const signUpForm = document.getElementById("sign-up-form");
signUpForm.addEventListener("submit", e=>handleForm(e, "Invalid input!"));