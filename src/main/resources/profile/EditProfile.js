function menuToggle(){
    const toggleMenu = document.querySelector('.menu');
    toggleMenu.classList.toggle('active');
}

async function getUserCredentials() {
    let url = "http://localhost:9001/EditProfile"
    let response = await fetch(url, {method: "PUT"})

    let data = await response.json()
    
	let emailBox = document.getElementById("emailBox")
	let usernameBox = document.getElementById("usernameBox")
	
	emailBox.setAttribute("value", data["email"])
	usernameBox.setAttribute("value", data["username"])
}

async function changeUserCredentials(event) {
    let url = "http://localhost:9001/EditProfile"
    let response = await fetch(url, {method: "POST"})

    let data = await response.json()
    
	if(data["status"] == true){
		alert("Success. User Credentials have been changed.")
		//window.location.replace("http://localhost:9001/home")
	} else {
		alert("Error. User Credentials have not been changed. Please check password and try again.")
		//window.location.replace("http://localhost:9001/EditProfile")
	}
}

async function postFormDataAsJson(url, formData) {
	// const plainFormData = Object.fromEntries(formData.entries());
	// const formDataJsonString = JSON.stringify(plainFormData);
	// const formDataJsonString = JSON.stringify(formData);

	console.log(formData);
	const fetchOptions = {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			"Accept": "application/json",
		},
		body: JSON.stringify(formData),
	};
	console.log(fetchOptions);

	const response = await fetch(url, fetchOptions);
	return response.ok;
}

async function handleForm(event) {
	event.preventDefault();

	const form = event.currentTarget;
	const url = form.action;

	formJson = {};
	for (ele of form.getElementsByTagName("input")) {
		if (ele.type != "submit")
			formJson[ele.name] = ele.value
	}
    const responseStatus = await postFormDataAsJson(url, formJson);
    if (!responseStatus) {
		alert("Error. User Credentials have not been changed. Please check password and try again.")
		//window.location.replace("http://localhost:9001/EditProfile")
    } else {
		alert("Success. User Credentials have been changed.")
		window.location.replace("http://localhost:9001/home")
	}
}



document.getElementById("changeForm").addEventListener("submit", event=>handleForm(event));


  function confirmDelete(){
    let answer = window.confirm(" Are you sure you want to delete your account?");
    if (answer){
    	let url = "http://localhost:9001/deleteAcc"
      	fetch(url, {
		    method: 'DELETE'})
		  .then(response => response.json())
		  .then(data => {
			console.log(data)
		  })
		window.location.replace("http://localhost:9001/login")
    }
    else {
      // None
    }
  }
  
  

  
window.onload = function() {
    this.getUserCredentials();
}