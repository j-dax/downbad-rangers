DROP TABLE IF EXISTS Stegano_users, Stegano_images, Stegano_BirdPeople CASCADE

CREATE TABLE Stegano_users(
	user_id bigserial,
	user_name varchar UNIQUE,
	user_pass varchar,
	email varchar,
	PRIMARY KEY (user_id)
)

CREATE TABLE Stegano_images(
	user_id bigserial,
	img_id bigserial unique,
	img_link varchar,
	PRIMARY KEY (img_id),
	FOREIGN KEY (user_id) REFERENCES Stegano_users(user_id)
	ON DELETE CASCADE
)

CREATE TABLE Stegano_BirdPeople (
	user_id bigserial PRIMARY KEY
		REFERENCES Stegano_users(user_id),
	user_name varchar UNIQUE
		REFERENCES Stegano_users(user_name)
		ON DELETE CASCADE,
	user_token varchar,
	user_verifier varchar
)

-- DROP TABLE <table> || Removes the table
-- TRUNCATE TABLE <table> || clears rows in table ||
-- Update <table> SET <column> = 'condition' WHERE <column> = 'value'
-- DELETE FROM <table> WHERE 'condition' 
-- INSERT INTO <table> ('column1','column2'...) VALUES (default,'value1'...) || Can use default for auto increment forms
-- ALTER TABLE <table>
	-- DROP COLUMN <column>
	-- ADD COLUMN <column> <datatype>

-- INSERT INTO Stegano_users VALUES (default, 'lilbabyman', 'dababy', 'dababy@gmail.com')

-- INSERT INTO Stegano_images VALUES (1, default, 'carp2.com/uthe.jpg')