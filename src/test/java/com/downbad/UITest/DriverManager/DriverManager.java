package com.downbad.UITest.DriverManager;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverManager {
    private static FirefoxDriver driverSingleton = null;

    private DriverManager() {}

    public static WebDriver getInstance() {
        if (driverSingleton == null) {
            driverSingleton = new FirefoxDriver();
            driverSingleton.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
        return driverSingleton;
    }
    
}
