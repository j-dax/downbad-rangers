package com.downbad.UITest;

import com.downbad.models.pages.POMAbstract;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;

public abstract class TestPOMPage {
    protected WebDriver driver;
    protected POMAbstract page;

    @BeforeMethod
    public void goTo() {
        page.getPage();
    }
}
