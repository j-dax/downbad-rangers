package com.downbad.UITest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;

/**
 * Might be nice if we want to setup headless testing on our pipeline,
 * but probably not
 */
public class WebDriverDataProvider {
    @DataProvider(name = "browsers")
    public Object[][] getBrowsers() {
        WebDriver[][] drivers = {
            {new FirefoxDriver()},
            {new ChromeDriver()}
        };
        return drivers;
    }
}
