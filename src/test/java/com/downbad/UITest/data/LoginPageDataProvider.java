package com.downbad.UITest.data;

import org.testng.annotations.DataProvider;

public class LoginPageDataProvider {
    
    @DataProvider(name = "signInData")
    public Object[][] signInData() {
        // signIn(CharSequence username, CharSequence password)
        Object[][] obj = {
            {"", ""}, // intentional failure
            {"admin", "admin"},
            {"jimmy", "verysecurepassword"},
        };
        return obj;
    }

    @DataProvider(name = "signUpData")
    public Object[][] signUpData() {
        Object[][] obj = {
            // signUp(CharSequence username, CharSequence email, CharSequence password)
            {"", "", ""}, // intentional failure
            {"admin", "admin@admin.com", "admin"},
            {"jimmy", "jchoo@chooshoes.com", "verysecurepassword"},
        };
        return obj;
    }
    
}
