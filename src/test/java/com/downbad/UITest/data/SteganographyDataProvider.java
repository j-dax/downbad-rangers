package com.downbad.UITest.data;

import org.testng.annotations.DataProvider;

public class SteganographyDataProvider {
    @DataProvider(name = "encodeMessageData")
    public Object[][] encodeMessageOnImage() {
        Object[][] obj = {
            {"C:/Users/EzriDax/workspace/revature/downbad-rangers/src/test/java/com/downbad/unitTest/test.png", "My secret message"}
        };
        return obj;
    }

    @DataProvider(name = "decodeMessageData")
    public Object[][] decodeMessageDataFromImage() {
        Object[][] obj = {
            {"C:/Users/EzriDax/workspace/revature/downbad-rangers/src/test/java/com/downbad/unitTest/test_decode.png", "My secret message"}
        };
        return obj;
    }
}
