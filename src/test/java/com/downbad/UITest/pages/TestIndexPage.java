package com.downbad.UITest.pages;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import com.downbad.UITest.TestPOMPage;
import com.downbad.UITest.DriverManager.DriverManager;
import com.downbad.models.components.Navbar;
import com.downbad.models.pages.IndexPage;

import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestIndexPage extends TestPOMPage {

    @BeforeClass
    public void onClassStart() {
        page = new IndexPage(DriverManager.getInstance());
    }

    @BeforeMethod
    public void goToPage() {
        driver.get(page.getPage());
    }

    @Test
    public void testNavigation() {
        // get the navbar list items
        List<WebElement> anchorElements = ((Navbar) page).getNavbarLinks();
        Assert.assertEquals(anchorElements.size(), 6);

        // verify content
        HashSet<String> expectedValues = new HashSet<>();
        Collections.addAll(expectedValues,
                        "Logout", "About Us", "Decrypt",
                        "Encrypt", "Collection", "Home" 
                        );
        for (WebElement we: anchorElements) {
            Assert.assertTrue(expectedValues.contains(we.getText()));
        }
    }

    @Test
    public void testNavigationLinkHome() {
        Navbar nav = (Navbar) page;
        nav.home().click();
        Assert.assertNotEquals(driver.getCurrentUrl(), page.getPage());
    }

    @Test
    public void testNavigationLinkAboutUs() {
        Navbar nav = (Navbar) page;
        nav.aboutUs().click();
        Assert.assertNotEquals(driver.getCurrentUrl(), page.getPage());
    }

    @Test
    public void testNavigationLinkCollection() {
        Navbar nav = (Navbar) page;
        nav.collection().click();
        Assert.assertNotEquals(driver.getCurrentUrl(), page.getPage());
    }

    @Test
    public void testNavigationLinkDecrypt() {
        Navbar nav = (Navbar) page;
        nav.decrypt().click();
        Assert.assertNotEquals(driver.getCurrentUrl(), page.getPage());
    }

    @Test
    public void testNavigationLinkEncrypt() {
        Navbar nav = (Navbar) page;
        nav.encrypt().click();
        Assert.assertNotEquals(driver.getCurrentUrl(), page.getPage());
    }

    @Test
    public void testNavigationLinkLogout() {
        Navbar nav = (Navbar) page;
        nav.logout().click();
        Assert.assertNotEquals(driver.getCurrentUrl(), page.getPage());
    }
    
    @Test
    public void testAlternativeFlowEncrypt() {
        ((IndexPage) page).clickAlternateEncryptButton();
        Assert.assertNotEquals(driver.getCurrentUrl(), page.getPage());
    }
    
    @Test
    public void testAlternativeFlowDecrypt() {
        ((IndexPage) page).clickAlternateDecryptButton();
        Assert.assertNotEquals(driver.getCurrentUrl(), page.getPage());
    }

    @AfterClass
    public void onClassComplete() {
        driver.quit();
    }
}
