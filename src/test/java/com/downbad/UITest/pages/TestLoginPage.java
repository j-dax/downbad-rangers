package com.downbad.UITest.pages;

import com.downbad.UITest.TestPOMPage;
import com.downbad.UITest.DriverManager.DriverManager;
import com.downbad.models.pages.LoginPage;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestLoginPage extends TestPOMPage {

    @BeforeClass
    public void onClassStart() {
        page = new LoginPage(DriverManager.getInstance());
    }

    @BeforeMethod
    public void goToPage() {
        driver.get(page.getPage());
    }
    
    @Test(dataProvider = "signUpData")
    public void testSignUp(CharSequence username, CharSequence email, CharSequence password) {
        ((LoginPage) page).rollRight();
        ((LoginPage) page).signUp(username, email, password); // sends click
        // do not redirect on invalid values
        if (username.length() == 0 || email.length() == 0 || password.length() == 0)
            Assert.assertEquals(driver.getCurrentUrl(), page.getPage()); 
        else
            Assert.assertNotEquals(driver.getCurrentUrl(), page.getPage());
    }
    
    @Test(dataProvider = "signInData")
    public void testSignIn(CharSequence username, CharSequence password) {
        ((LoginPage) page).signIn(username, password);
        if (username.length() == 0 || password.length() == 0)
            Assert.assertEquals(driver.getCurrentUrl(), page.getPage()); 
        else
            Assert.assertNotEquals(driver.getCurrentUrl(), page.getPage());
    }
    
    @Test
    public void testRollLeftOccurs() {
        ((LoginPage) page).rollLeft();
    }
    
    @Test
    public void testRollRightOccurs() {
        ((LoginPage) page).rollRight();
    }

    @AfterClass
    public void onClassComplete() {
        driver.quit();
    }
}
