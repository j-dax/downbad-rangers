package com.downbad.models.components;

import java.util.List;

import com.downbad.models.pages.POMAbstract;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class Navbar extends POMAbstract {
    public Navbar(WebDriver wd, String pagePath) {
        super(wd, pagePath);
    }

    public List<WebElement> getNavbarLinks() {
        return driver.findElements(By.cssSelector(".nav-header > ul:nth-child(1) > li > a")); 
    }

    public WebElement home() {
        return driver.findElement(By.cssSelector(".nav-header > ul:nth-child(1) > li:nth-child(1)"));
    }

    public WebElement encrypt() {
        return driver.findElement(By.cssSelector(".nav-header > ul:nth-child(1) > li:nth-child(2)"));
    }

    public WebElement decrypt() {
        return driver.findElement(By.cssSelector(".nav-header > ul:nth-child(1) > li:nth-child(3)"));
    }

    public WebElement collection() {
        return driver.findElement(By.cssSelector(".nav-header > ul:nth-child(1) > li:nth-child(4)"));
    }

    public WebElement aboutUs() {
        return driver.findElement(By.cssSelector(".nav-header > ul:nth-child(1) > li:nth-child(5)"));
    }

    public WebElement logout() {
        return driver.findElement(By.cssSelector(".nav-header > ul:nth-child(1) > li:nth-child(6)"));
    }
}
