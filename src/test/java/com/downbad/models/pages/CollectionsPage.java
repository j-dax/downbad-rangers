package com.downbad.models.pages;

import com.downbad.models.components.Navbar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CollectionsPage extends Navbar {
    public final static String urlPath = "file:///C:/Users/EzriDax/workspace/revature/downbad-rangers/src/main/resources/views/Collection.html";
    // final static String urlPath = "/index.html"; // DELETE the line above when the backend has consistent routing
    public CollectionsPage(WebDriver wd) {
        super(wd, urlPath);
    }

    public WebElement getSlider() {
        return driver.findElement(By.cssSelector(".slider"));
    }
    
    public WebElement getDownloadButton() {
        return driver.findElement(By.cssSelector(".decrypt-button-full"));
    }
    
    public WebElement getShareOnTwitterButton() {
        return driver.findElement(By.cssSelector(".fab.fa-twitter"));
    }
    
}
