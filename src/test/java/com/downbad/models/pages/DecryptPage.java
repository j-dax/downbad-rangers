package com.downbad.models.pages;

import com.downbad.models.components.Navbar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DecryptPage extends Navbar {
    public final static String urlPath = "file:///C:/Users/EzriDax/workspace/revature/downbad-rangers/src/main/resources/views/decrypt.html";
    // final static String urlPath = "/index.html"; // DELETE the line above when the backend has consistent routing
    
    public DecryptPage(WebDriver wd) {
        super(wd, urlPath);
    }

    public void uploadImage(CharSequence absoluteFilepath) {
        WebElement fileUpload = driver.findElement(By.className("drop-zone"));
        fileUpload.click();
        // keep this frame of reference and instruct explorer where to find the file
        fileUpload.sendKeys(absoluteFilepath);
        // didnt we decide to remove this button?
        driver.findElement(By.cssSelector(".decrypt-button-full")).click();
    }

    public String getSecretMessageContent() {
        WebElement messageBox = driver.findElement(By.cssSelector(".column-3 > div:nth-child(1) > textarea:nth-child(2)"));
        return messageBox.getText();
    }
    
}
