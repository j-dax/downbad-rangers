package com.downbad.models.pages;

import com.downbad.models.components.Navbar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EncryptPage extends Navbar {
    public final static String urlPath = "file:///C:/Users/EzriDax/workspace/revature/downbad-rangers/src/main/resources/views/encrypt.html";
    // final static String urlPath = "/index.html"; // DELETE the line above when the backend has consistent routing
    
    public EncryptPage(WebDriver wd) {
        super(wd, urlPath);
    }

    void uploadImage(CharSequence filepath) {
        WebElement fileUpload = driver.findElement(By.className("drop-zone"));
        fileUpload.click();
        // keep this frame of reference and instruct explorer where to find the file
        fileUpload.sendKeys(filepath);
    }
    
    void setMessage(CharSequence message) {
        driver.findElement(
            By.cssSelector(".column-3 > div:nth-child(1) > textarea:nth-child(2)"))
        .sendKeys(message);
    }

    public void encodeMessageOnImage(CharSequence filepath, CharSequence message) {
        uploadImage(filepath);
        setMessage(message);
        driver.findElement(By.cssSelector("encrypt-button-full")).click();
    }
}
