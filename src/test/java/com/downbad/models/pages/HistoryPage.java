package com.downbad.models.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HistoryPage extends POMAbstract {
    public final static String urlPath = "file://C:/Users/EzriDax/workspace/revature/downbad-rangers/src/main/resources/views/history.html";
    // final static String urlPath = "/index.html"; // DELETE the line above when the backend has consistent routing
    
    public HistoryPage(WebDriver wd) {
        super(wd, urlPath);
    }

    public String getH2() {
        return driver.findElement(By.tagName("h2")).getText();
    }
}
