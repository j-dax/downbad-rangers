package com.downbad.models.pages;

import com.downbad.models.components.Navbar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class IndexPage extends Navbar {
    public final static String urlPath = "file://C:/Users/EzriDax/workspace/revature/downbad-rangers/src/main/resources/views/index.html";
    // final static String urlPath = "/index.html"; // DELETE the line above when the backend has consistent routing
    
    public IndexPage(WebDriver wd) {
        super(wd, urlPath);
    }

    public void clickAlternateEncryptButton() {
        driver.findElement(By.className("encrypt-button")).click();
    }

    public void clickAlternateDecryptButton() {
        driver.findElement(By.className("decrypt-button")).click();
    }
    
}
