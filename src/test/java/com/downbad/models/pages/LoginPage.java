package com.downbad.models.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends POMAbstract {
    public LoginPage(WebDriver wd) {
        super(wd, "/login.html");
    }

    public void signIn(CharSequence username, CharSequence password) {
        driver.findElement(
            By.cssSelector("#sign_in_form > div:nth-child(3) > input:nth-child(2)"))
        .sendKeys(username);
        driver.findElement(
            By.cssSelector("#sign_in_form > div:nth-child(2) > input:nth-child(2)"))
        .sendKeys(password);
        driver.findElement(
            By.cssSelector("input.btn:nth-child(4)"))
        .click();
    }

    public void signUp(CharSequence username, CharSequence email, CharSequence password) {
        driver.findElement(By.cssSelector(".sign-up-form > div:nth-child(2)"))
        .sendKeys(username);
        driver.findElement(By.cssSelector(".sign-up-form > div:nth-child(3)"))
        .sendKeys(email);
        driver.findElement(By.cssSelector(".sign-up-form > div:nth-child(4)"))
        .sendKeys(password);
        driver.findElement(By.cssSelector("input.btn:nth-child(5)"))
        .click();
    }

    public void rollLeft() {
        driver.findElement(By.id("sign-up-button")).click();
    }

    public void rollRight() {
        driver.findElement(By.id("sign-in-button")).click();
    }

}
