package com.downbad.models.pages;


import org.openqa.selenium.WebDriver;

public class POMAbstract {
    protected WebDriver driver;
    protected String pagePath;

    @SuppressWarnings("unused")
    private POMAbstract() {}
    
    public POMAbstract(WebDriver wd, String pagePath) {
        this.driver = wd;
        this.pagePath = pagePath;
    }

    public String getPage() {
        return pagePath;
    }

    public String getTitle() {
        return driver.getTitle();
    }
}
