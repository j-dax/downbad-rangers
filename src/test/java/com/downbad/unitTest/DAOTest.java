package com.downbad.unitTest;

import config.ConnectionConfig;
import config.ResourceClosers;
import org.testng.Assert;
import org.testng.annotations.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DAOTest {


    @BeforeMethod(groups = {"requireDB"})
    public void beforeMethod () {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            //Establish the connection to the DB
            conn = ConnectionConfig.getTestConnection();
            stmt = conn.prepareStatement("INSERT into stegano_users VALUES(default, ?, ?, ?)");
            stmt.setString(1, "test");
            stmt.setString(2, "test");
            stmt.setString(3, "test.com");
            stmt.execute();
            stmt = conn.prepareStatement( "INSERT into stegano_images VALUES(?, default, ?)");
            stmt.setInt(1, 1);
            stmt.setString(2, "test.png");
            stmt.execute();

        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            ResourceClosers.closeConnection(conn);
            ResourceClosers.closeStatement(stmt);
        }

    }

    @Test(groups = {"requireDB"})
    public void testLoginUser () {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet result;
        String username = "test";
        String password = "test";
        try {
            //Establish the connection to the DB
            conn = ConnectionConfig.getTestConnection();
            final String SQL = "SELECT * FROM stegano_users where user_name=? AND user_pass=?";
            stmt = conn.prepareStatement(SQL);
            //Since the SQL statement is parameterized, I need to set the values of
            //the parameters.
            stmt.setString(1, username);
            stmt.setString(2, password);
            //And of course, execute the SQL statement once you have set your parameters.
            result = stmt.executeQuery();

            Assert.assertTrue(result.next());

        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            ResourceClosers.closeConnection(conn);
            ResourceClosers.closeStatement(stmt);
        }
    }

    @Test(groups = {"requireDB"})
    public void testSignUpUser() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet result;
        String username = "new user";
        String password = "password";
        String email = "user.com";

        try {
            //Establish the connection to the DB
            conn = ConnectionConfig.getTestConnection();
            final String SQL = "INSERT into stegano_users VALUES(default, ?, ?, ?) RETURNING (user_id)";
            stmt = conn.prepareStatement(SQL);
            //Since the SQL statement is parameterized, I need to set the values of
            //the parameters.
            stmt.setString(1, username);
            stmt.setString(2, password);
            stmt.setString(3, email);
            //And of course, execute the SQL statement once you have set your parameters.

            result = stmt.executeQuery();

            Assert.assertTrue(result.next());

        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            ResourceClosers.closeConnection(conn);
            ResourceClosers.closeStatement(stmt);
        }
    }

    @Test(groups = {"requireDB"})
    public void testEncode() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet result = null;
        int user_id = 1;
        String filepath = "newImage.png";

        try {
            //Establish the connection to the DB
            conn = ConnectionConfig.getTestConnection();
            final String SQL = "INSERT into stegano_images VALUES(?, default, ?) RETURNING(img_id)";
            stmt = conn.prepareStatement(SQL);
            //Since the SQL statement is parameterized, I need to set the values of
            //the parameters.
            stmt.setInt(1, user_id);
            stmt.setString(2, filepath);
            //And of course, execute the SQL statement once you have set your parameters.
            result = stmt.executeQuery();

            Assert.assertTrue(result.next());

        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            ResourceClosers.closeConnection(conn);
            ResourceClosers.closeStatement(stmt);
        }
    }

    @Test(groups = {"requireDB"})
    public void testGetNextImgId() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet result = null;

        try {
            //Establish the connection to the DB
            conn = ConnectionConfig.getTestConnection();
            final String SQL = "Select max(img_id) as max_img_id from stegano_images";
            stmt = conn.prepareStatement(SQL);
            result = stmt.executeQuery();
            if(result.next()) {
                Assert.assertEquals(result.getInt("max_img_id") + 1, 2);
            }
            else {
                Assert.fail("No img in the database");
            }

        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            ResourceClosers.closeConnection(conn);
            ResourceClosers.closeStatement(stmt);
        }
    }

    @Test(groups = {"requireDB"})
    public void testGetImages() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet result;
        Map<Integer,String> images = new HashMap<>();
        int user_id = 1;

        try {
            //Establish the connection to the DB
            conn = ConnectionConfig.getTestConnection();
            final String SQL = "Select img_id, img_link from stegano_images where user_id=?";
            stmt = conn.prepareStatement(SQL);

            stmt.setInt(1, user_id);
            result = stmt.executeQuery();

            while(result.next()) {
                images.put(result.getInt("img_id"), result.getString("img_link"));
            }

        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            ResourceClosers.closeConnection(conn);
            ResourceClosers.closeStatement(stmt);
        }
        Assert.assertEquals(images.get(1), "test.png");
        Assert.assertNull(images.get(2));
    }

    @AfterMethod(groups = {"requireDB"})
    public void afterMethod() {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            //Establish the connection to the DB
            conn = ConnectionConfig.getTestConnection();
            stmt = conn.prepareStatement("TRUNCATE TABLE stegano_users RESTART IDENTITY CASCADE");
            stmt.execute();

        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            ResourceClosers.closeConnection(conn);
            ResourceClosers.closeStatement(stmt);
        }
    }
}
