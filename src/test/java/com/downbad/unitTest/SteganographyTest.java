package com.downbad.unitTest;

import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import stegan.Steganography;


import java.io.File;

public class SteganographyTest {
    Steganography stego = new Steganography();

    @AfterSuite
    public void afterSuite() {
        File file = new File("src/test/java/com/downbad/unitTest/test_encode.png");
        if (file.delete()) {
            System.out.println("Deleted the file: " + file.getName());
        } else {
            System.out.println("Failed to delete the file.");
        }
    }

    @Test
    public void testEncodeImage() {
        Assert.assertTrue(stego.encode("src/test/java/com/downbad/unitTest", "test.png", "png", "test_encode.png", "This is a test"));
    }

    @Test
    public void testDecodeImage() {
        Assert.assertEquals(stego.decode("src/test/java/com/downbad/unitTest", "test_decode.png"), "This is a test");
    }

}
